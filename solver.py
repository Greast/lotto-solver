#!/usr/bin/python
from bitarray import bitarray # pip install bitarray
from itertools import combinations
from multiprocessing import Pool
import copy,os
def solve(l,start,key = lambda c,k : -sum(c | ~k)):
    m = list(enumerate(l))
    k = copy.copy(start)
    while not all(k):
        ma = max(enumerate(m),key=lambda x : key(x[1][1],k))
        del m[ma[0]]
        k|= ma[1][1]
        yield ma[1][0]

def mmatrix(n,k,p,t,f = lambda p,k,t : len(p & k) >= t):
# N/v : Range of numbers.
# K : Numbers pulled by the lotto.
# P : User coupon size.
# T : Needed matches for a win.
# K = P for all CCRWEST cases.
    coupons = list(combinations(n,p))
    draws   = list(combinations(n,k))
    l=[]
    for c in enumerate(coupons):
        l.append(bitarray([f(set(d),set(c[1]),t) for d in draws]))
    return l

class matrix:
    def __init__(self,n,k,p,t,f = lambda p,k,t : len(set(p) & set(k)) >= t):
        def g(v,l):
            return list(enumerate( (s for s in combinations(v,l))))
        self.coupons = g(n,p)
        self.draws = g(n,k) if k is not p else self.coupons
        self.f = f
        self.t = t

    def map (self, c, k):
        return (self.f(d[1],c,self.t) if b else False for b,d in zip(k,self.draws))

    def calc(self, c):
        return (self.f(d[1],c,self.t) for d in self.draws)

    def __getitem__(self,i, k = None):
        return self.map(coupons[i][1],k) if k else self.calc(coupons[i][1])

    def pmax(self, k =None, blacklist=None):
        blacklist = [0] * len(self.coupons)
        def f(c):
            return sum(self.calc(c))
        def fk(c):
            return sum(self.map(c,k))
        g = fk if k else f
        return max(((c[0],c[1],g(c[1])) for c in self.coupons if not blacklist[c[0]]), key = lambda x : - x[-1])

    def solve(self):
        k = bitarray([0] * len(self.draws))
        blacklist = [0] * len(self.coupons)
        while not all(k):
            m = self.pmax(k,blacklist)
            blacklist[m[0]]=1
            k|=bitarray(self.calc(m[1]))
            yield m[0]


if __name__ == "__main__" :
    import os,sys,argparse
    parser = argparse.ArgumentParser(description='Solve given lotto combination')
    parser.add_argument("Range",    metavar="N",type=int)
    parser.add_argument("Draws",    metavar="K",type=int)
    parser.add_argument("Coupons",  metavar="P",type=int)
    parser.add_argument("Match",    metavar="T",type=int)
    parser.add_argument('-mem',     action='store_true')
    args = parser.parse_args()
    coupons = list(combinations(range(1,args.Range+1),args.Coupons))
    if args.mem:
        a = matrix(range(1,args.Range+1),args.Draws,args.Coupons,args.Match)
        S = a.solve()
    else:
        a = mmatrix(range(1,args.Range+1),args.Draws,args.Coupons,args.Match)
        S = solve(a,bitarray([False]*len(a[0])))

    print(os.linesep.join(("{}".format(coupons[x]) for x in S)))
