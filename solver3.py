from itertools import combinations
def check(a,b,t):
    return all(any(len(x & y) >= t for y in b) for x in a)
