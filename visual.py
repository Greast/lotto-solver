import matplotlib
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
from itertools import combinations

if __name__ == "__main__":
    import os,sys,argparse
    parser = argparse.ArgumentParser(description='Solve given lotto combination')
    parser.add_argument("Range",    metavar="N",type=int)
    parser.add_argument("Draws",    metavar="K",type=int)
    parser.add_argument("Coupons",  metavar="P",type=int)
    args = parser.parse_args()

    draws = list((set(x) for x in combinations(range(1,args.Range+1),args.Draws)))
    coupons = list((set(x) for x in combinations(range(1,args.Range+1),args.Coupons)))
    def fun(x, y):
      return len(x & y)

    # Make data.
    a=[[ fun(x,y) for x in coupons] for y in draws ]
    plt.imshow(a, cmap='hot', interpolation='nearest')
    plt.savefig("imgL({},{},{}).png".format(args.Range,args.Draws,args.Coupons))
    plt.show()
