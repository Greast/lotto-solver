#!/usr/bin/python
def check(v,l,t):
    return len(cover(v,l,t)) is 0
def cover(v,l,t):
    R=[]
    V = [set(x) for x in v]
    for x in (set(x) for x in l):
        b = False
        for y in V:
            if len(x & y) >= t:
                b = True
                #print("{} U {} >= {}".format(x,y,t))
                break

        if not b:
            R.append(x)
    return R


if __name__ == "__main__":
    from ast import literal_eval
    from itertools import combinations
    import argparse,sys
    parser = argparse.ArgumentParser(description='Solve given lotto combination')
    parser.add_argument("Range",    metavar="N",type=int)
    parser.add_argument("Lotto",    metavar="P",type=int)
    parser.add_argument("Match",    metavar="T",type=int)
    parser.add_argument('-v',     action='store_true')
    args = parser.parse_args()
    L = combinations(range(1,args.Range+1),args.Lotto)
    V = (set(literal_eval(x)) for x in sys.stdin)
    f = cover if args.v else check
    print(f(V,L,args.Match))
