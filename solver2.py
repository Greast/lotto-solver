#!/usr/bin/python3
import copy
from itertools import combinations
def bitfield(n):
    return (int(digit) for digit in bin(n)[2:])
def compress(booleans,data):
    return (d for b,d in zip(booleans,data) if b)
def check(a,b,t):
    return all(any(len(x & y) >= t for y in b) for x in a)

if __name__ == "__main__" :
    import os,sys,argparse
    parser = argparse.ArgumentParser(description='Solve given lotto combination')
    parser.add_argument("Range",    metavar="N",type=int)
    parser.add_argument("Draws",    metavar="K",type=int)
    parser.add_argument("Coupons",  metavar="P",type=int)
    parser.add_argument("Match",    metavar="T",type=int)
    parser.add_argument('-mem',     action='store_true')
    args = parser.parse_args()

    a = list(set(x) for x in combinations(range(args.Range),args.Coupons))
    b = list(set(x) for x in combinations(range(args.Range),args.Draws))
    best = 2**len(a) - 1
    m = sum(bitfield(best))
    for i in range(best -1, 0 ,-1):
        l = list(bitfield(i))
        s = [0] * (len(a) - len(l)) + l
        if check(compress(s,a),b,args.Match):
            #print("Yes {}".format(list(compress(s,a))))
            su = sum(s)
            if su < m:
                best = i
                m = su
    print("\n".join((str(x) for x in compress(bitfield(best),a))))
