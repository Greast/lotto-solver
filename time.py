#!/usr/bin/python
import time
def runtime(f):
    st = time.time()
    R = f()
    return (time.time()-st,R)


if __name__ == '__main__':
    import os, argparse,shlex,subprocess
    parser = argparse.ArgumentParser(description='Caluclate runtime of a given program')
    parser.add_argument("Command", metavar="P", type=str)
    parser.add_argument("Range", metavar="R", type=str)
    parser.add_argument('--times', metavar="-T", default=1, type=int)
    parser.add_argument('--format', metavar="-f", default="{}", type=str)
    parser.add_argument('--avg',action='store_true')
    args = parser.parse_args()
    def f(x):
        return runtime(
            lambda : subprocess.Popen(
                shlex.split(
                    args.Command.format(x)
                )
                #, stdout=subprocess.PIPE
                )
            )[0]


    if not args.avg:
        print(os.linesep.join((("\t".join((args.format.format(f(V)))for V in shlex.split(args.Range))) for t in range(args.times))))
    else :
        print(os.linesep.join((args.format.format(sum(x)/len(x)) for x in
            [[f(V) for t in range(args.times) ] for V in shlex.split(args.Range)]
        )))
